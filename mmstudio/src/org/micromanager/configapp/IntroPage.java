///////////////////////////////////////////////////////////////////////////////
//FILE:          IntroPage.java
//PROJECT:       Micro-Manager
//SUBSYSTEM:     mmstudio
//-----------------------------------------------------------------------------
//
// AUTHOR:       Nenad Amodaj, nenad@amodaj.com, October 29, 2006
//
// COPYRIGHT:    University of California, San Francisco, 2006
//
// LICENSE:      This file is distributed under the BSD license.
//               License text is included with the source distribution.
//
//               This file is distributed in the hope that it will be useful,
//               but WITHOUT ANY WARRANTY; without even the implied warranty
//               of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//               IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//               CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//               INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES.
//
// CVS:          $Id: IntroPage.java 6334 2011-01-24 23:07:39Z arthur $
//
package org.micromanager.configapp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.prefs.Preferences;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import org.micromanager.MMStudioMainFrame;

import org.micromanager.utils.FileDialogs;
import org.micromanager.utils.ReportingUtils;
import javax.swing.JComboBox;

/**
 * The first page of the Configuration Wizard.
 */
public class IntroPage extends PagePanel {
   private static final long serialVersionUID = 1L;
   private ButtonGroup buttonGroup = new ButtonGroup();
   private boolean initialized_ = false;
   private JRadioButton modifyRadioButton_;
   private JRadioButton createNewRadioButton_;
   private static final String HELP_FILE_NAME = "conf_intro_page.html";
   private JComboBox configCombo_;
   
   /**
    * Create the panel
    */
   public IntroPage(Preferences prefs) {
      super();
      title_ = "Select the configuration file";
      helpText_ = "Welcome to the Micro-Manager Configurator.\n" +
                  "The Configurator will guide you through the process of configuring the software to work with your hardware setup.\n" +
                  "In this step you choose if you are creating a new hardware configuration or editing an existing one.";
      
      setLayout(null);
      prefs_ = prefs;
      setHelpFileName(HELP_FILE_NAME);

      createNewRadioButton_ = new JRadioButton();
      createNewRadioButton_.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent arg0) {
            model_.reset();
            initialized_ = false;
            configCombo_.setEnabled(false);
         }
      });
      buttonGroup.add(createNewRadioButton_);
      createNewRadioButton_.setText("Create new configuration");
      createNewRadioButton_.setBounds(10, 31, 424, 23);
      add(createNewRadioButton_);

      modifyRadioButton_ = new JRadioButton();
      buttonGroup.add(modifyRadioButton_);
      modifyRadioButton_.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent arg0) {
            configCombo_.setEnabled(true);
            
         }
      });
      modifyRadioButton_.setText("Modify or explore existing configuration");
      modifyRadioButton_.setBounds(10, 55, 424, 23);
      add(modifyRadioButton_);
      
      createNewRadioButton_.setSelected(true);
      
      configCombo_ = new JComboBox();
      configCombo_.setBounds(10, 94, 253, 22);
      add(configCombo_);
   }
   
   public void loadSettings() {
      // load settings
      //filePathField_.setText(prefs_.get(CFG_PATH, ""));
      if (model_ != null) {      
         if (model_.getConfigName().length() > 0) {
            modifyRadioButton_.setSelected(true);
            configCombo_.setEnabled(true);
            configCombo_.setSelectedItem(model_.getConfigName());
         }
      }
   }

   public void saveSettings() {
      // save settings
      //prefs_.put(CFG_PATH, filePathField_.getText());      
   }
   
   public boolean enterPage(boolean fromNextPage) {
      // collect all configuration files
      configCombo_.removeAllItems();
      File configFolder = new File("./config");
      File fileList[] = configFolder.listFiles();
      for (File f : fileList) {
         if (f.isFile()) {
            if (f.getName().toUpperCase().endsWith(new String(".cfg").toUpperCase())) {
               configCombo_.addItem(f.getName().substring(0, f.getName().lastIndexOf(".")));                  
            }  
         }
      }
      
      String curFileName = model_.getFileName();
      File curFile = new File(curFileName);
      if (curFile.exists()) {
         configCombo_.setSelectedItem(curFile.getName().substring(0, curFile.getName().lastIndexOf(".")));
      }
      
      if (modifyRadioButton_.isSelected()) {
         configCombo_.setEnabled(true);
      } else {
         configCombo_.setEnabled(false);
      }

      if (fromNextPage) {
         // if we are returning from the previous page clear everything and start all over
         model_.reset();
         try {
            core_.unloadAllDevices();
         } catch (Exception e) {
            ReportingUtils.showError(e);
         };
                  
         initialized_ = false;
      }
      return true;
   }

   public boolean exitPage(boolean toNextPage) {
      if (modifyRadioButton_.isSelected() && (!initialized_ || ((String)configCombo_.getSelectedItem()).compareTo(model_.getConfigName()) != 0)) {
         try {
            String cnfPath = "./config/" + (String)configCombo_.getSelectedItem() + ".cfg";
            model_.loadFromFile(cnfPath);
            model_.setConfigName((String)configCombo_.getSelectedItem());
         } catch (MMConfigFileException e) {
            ReportingUtils.showError(e);
            model_.reset();
            return false;
         }
         initialized_ = true;
      }
      return true;
   }
   

   public void refresh() {
   }
}
