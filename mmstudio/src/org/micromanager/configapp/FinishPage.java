///////////////////////////////////////////////////////////////////////////////
//FILE:          FinishPage.java
//PROJECT:       Micro-Manager
//SUBSYSTEM:     mmstudio
//-----------------------------------------------------------------------------
//
// AUTHOR:       Nenad Amodaj, nenad@amodaj.com, October 29, 2006
//
// COPYRIGHT:    University of California, San Francisco, 2006
//
// LICENSE:      This file is distributed under the BSD license.
//               License text is included with the source distribution.
//
//               This file is distributed in the hope that it will be useful,
//               but WITHOUT ANY WARRANTY; without even the implied warranty
//               of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//               IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//               CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//               INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES.
//
// CVS:          $Id: FinishPage.java 7241 2011-05-17 23:38:43Z karlh $
//
package org.micromanager.configapp;

import java.awt.Container;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import org.micromanager.MMStudioMainFrame;
import org.micromanager.utils.FileDialogs;

import org.micromanager.utils.GUIUtils;
import org.micromanager.utils.ReportingUtils;

/**
 * The last wizard page.
 *
 */
public class FinishPage extends PagePanel {

   private static final long serialVersionUID = 1L;
   private JTextField configNameField_;
   private boolean overwrite_ = false;

   /**
    * Create the panel
    */
   public FinishPage(Preferences prefs) {
      super();
      title_ = "Save configuration and exit";
      setHelpFileName("conf_finish_page.html");
      prefs_ = prefs;
      setLayout(null);

      final JLabel configurationWillBeLabel = new JLabel();
      configurationWillBeLabel.setText("Configuration file:");
      configurationWillBeLabel.setBounds(14, 11, 123, 21);
      add(configurationWillBeLabel);

      configNameField_ = new JTextField();
      configNameField_.setBounds(12, 30, 300, 24);
      add(configNameField_);

      //
   }

   public boolean enterPage(boolean next) {
      if (model_.getFileName().isEmpty()) {
         model_.setFileName("./config/NewConfig.cfg");
         model_.setConfigName("NewConfig");
      }

      configNameField_.setText(model_.getConfigName());
      return true;
   }


   public boolean exitPage(boolean toNext) {
      if(toNext)
         return saveConfiguration();

      return true;
   }

   public void refresh() {
   }

   public void loadSettings() {
      // TODO Auto-generated method stub
   }

   public void saveSettings() {
      // TODO Auto-generated method stub
   }

   private boolean saveConfiguration() {
      Container ancestor = getTopLevelAncestor();
      Cursor oldc = null;
      if (null != ancestor){
         oldc = ancestor.getCursor();
         Cursor waitc = new Cursor(Cursor.WAIT_CURSOR);
         ancestor.setCursor(waitc);
      }
      int sel = JOptionPane.YES_OPTION;

      try {
         core_.unloadAllDevices();
         GUIUtils.preventDisplayAdapterChangeExceptions();
         String fname = "./config/" + configNameField_.getText() + ".cfg";
         File f = new File(fname);
         if (f.exists() && !overwrite_) {
            sel = JOptionPane.showConfirmDialog(this,
                  "Overwrite " + f.getName() + "?",
                  "File Save",
                  JOptionPane.YES_NO_OPTION);
            if (sel == JOptionPane.YES_OPTION) {
               model_.removeInvalidConfigurations();
               model_.saveToFile(fname);
            }
         } else {
            model_.removeInvalidConfigurations();
            model_.saveToFile(fname);
         }
      } catch (MMConfigFileException e) {
         ReportingUtils.showError(e);
      } catch (Exception e) {
         ReportingUtils.showError(e);
      } finally{
         if (null != ancestor){
            if( null != oldc)
               ancestor.setCursor(oldc);
         }
      }
      return sel == JOptionPane.YES_OPTION ? true : false;
   }
}
